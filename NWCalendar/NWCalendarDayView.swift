//
//  NWCalendarDayView.swift
//  NWCalendarDemo
//
//  Created by Nicholas Wargnier on 7/24/15.
//  Copyright (c) 2015 Nick Wargnier. All rights reserved.
//

import Foundation
import UIKit

protocol NWCalendarDayViewDelegate {
  func dayButtonPressed(dayView: NWCalendarDayView)
}

class NWCalendarDayView: UIView {
  private let kDayFont             = UIFont(name: "Helvetica Neue", size: 14)
  private let kAvailableColor      = UIColor.blackColor()
  private let kNotAvailableColor   = UIColor.lightGrayColor()
  private let kNonActiveMonthColor = UIColor(red:0.949, green:0.949, blue:0.949, alpha: 1)
  private let kActiveMonthColor    = UIColor.whiteColor()
  private let kSelectedColor       = UIColor(red:0.988, green:0.325, blue:0.341, alpha: 1)
  
  var delegate : NWCalendarDayViewDelegate?
  var dayLabel : UILabel!
  var dayButton: UIButton!
  var eventDayLabel : UILabel!
    
  var date     : NSDate? {
    didSet {
      if let unwrappedDate = date {
        if unwrappedDate.nwCalendarView_dayIsInPast() {
          isInPast = true
        }
      }
    }
  }
  
  var day: NSDateComponents? {
    didSet {      
      date = day?.date
      dayButton.setTitle("\(day!.day)", forState: .Normal)
    }
  }
  
  var isActiveMonth = false {
    didSet {
      setNotSelectedBackgroundColor()
    }
  }
  
  var isInPast = false {
    didSet {
      isEnabled = !isInPast
    }
  }
  
  var isEnabled = true {
    didSet {
      dayButton.enabled = isEnabled        
    }
  }
  
  var isSelected = false {
    didSet {
      if isSelected {
        backgroundColor = UIColor.clearColor()
        dayButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        dayButton.setTitleColor(UIColor.redColor(), forState: .Disabled)
      } else {
        dayButton.setTitleColor(kAvailableColor, forState: .Normal)
        dayButton.setTitleColor(kNotAvailableColor, forState: .Disabled)
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
  override init(frame: CGRect) {
    
    super.init(frame: frame)
    backgroundColor = kNonActiveMonthColor
    
    dayButton = UIButton(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
    dayButton.titleLabel?.textAlignment = .Center
    dayButton.titleLabel?.font = kDayFont
    dayButton.setTitleColor(kAvailableColor, forState: .Normal)
    dayButton.setTitleColor(kNotAvailableColor, forState: .Disabled)
    dayButton.addTarget(self, action: "dayButtonPressed:", forControlEvents: .TouchUpInside)
    addSubview(dayButton)
    
    eventDayLabel = UILabel(frame: CGRect(x: dayButton.frame.size.width / 2 - 3, y: dayButton.frame.size.height - 5, width: 5, height: 5))
    eventDayLabel.layer.cornerRadius = 2.5
    eventDayLabel.layer.masksToBounds = true
  }

  func dayButtonPressed(sender: AnyObject) {
    delegate?.dayButtonPressed(self)
  }
  
  func setDayForDay(day: NSDateComponents) {
    self.day = day.date!.nwCalendarView_dayWithCalendar(day.calendar!)
  }
  
  func setNotSelectedBackgroundColor() {
    if !isSelected {
      if isActiveMonth {
        eventDayLabel.backgroundColor = UIColor.darkGrayColor()
        backgroundColor = kActiveMonthColor
        eventDayLabel .removeFromSuperview()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if appDelegate.eventDatesArray .containsObject((dayButton.titleLabel?.text)!)
        {
            addSubview(eventDayLabel)
        }
        
      } else {
        eventDayLabel.backgroundColor = UIColor.clearColor()
        backgroundColor = kNonActiveMonthColor
      }
    }
  }
}