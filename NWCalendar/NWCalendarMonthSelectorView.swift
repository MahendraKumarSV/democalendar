//
//  NWCalendarMonthSelectorView.swift
//  NWCalendarDemo
//
//  Created by Nicholas Wargnier on 7/24/15.
//  Copyright (c) 2015 Nick Wargnier. All rights reserved.
//

import UIKit
import Foundation

class NWCalendarMonthSelectorView: UIView {
  private let kPrevButtonImage:UIImage! = UIImage(named: "leftArrowImg")
  private let kNextButtonImage:UIImage! = UIImage(named: "rightArrowImg")
  private let kMonthColor:UIColor!      = UIColor.darkGrayColor()
  private let kMonthFont:UIFont!        = UIFont(name: "HelveticaNeue-Bold", size: 16)
  private let kSeperatorColor:UIColor!  = UIColor(red:0.973, green:0.973, blue:0.973, alpha: 1)
  private let kSeperatorWidth:CGFloat!  = 1.5
  
  var prevButton: UIButton!
  var nextButton: UIButton!
  var monthLabel: UILabel!
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    let buttonWidth = floor(frame.width/9)
    prevButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 23))
    prevButton.setImage(kPrevButtonImage, forState: .Normal)

    nextButton = UIButton(frame: CGRect(x: frame.width-buttonWidth - 20, y: 0, width: 50, height: 23))
    nextButton.setImage(kNextButtonImage, forState: .Normal)

    monthLabel = UILabel(frame: CGRect(x: buttonWidth, y: 0, width: frame.width-(2*buttonWidth), height: frame.height))
    monthLabel.textAlignment = .Center
    monthLabel.textColor = kMonthColor
    monthLabel.font = kMonthFont
    monthLabel.tag = 1000
    
    addSubview(prevButton)
    addSubview(nextButton)
    addSubview(monthLabel)
    
    addSeperator(0)
    addSeperator(frame.height-kSeperatorWidth)
  }
  
  private func addSeperator(y: CGFloat) {
    let seperator = CALayer()
    seperator.backgroundColor = kSeperatorColor.CGColor
    seperator.frame = CGRect(x: 0, y: y, width: frame.width, height: kSeperatorWidth)
    layer.addSublayer(seperator)
  }
}

extension NWCalendarMonthSelectorView {
  func updateMonthLabelForMonth(month: NSDateComponents) {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "MMMM"
    let date = month.calendar?.dateFromComponents(month)
    monthLabel.text = formatter.stringFromDate(date!)
  }
}
