//
// MasterViewController.swift
// EventCalendar
//

import UIKit
import JLToast

enum JSONError: String, ErrorType
{
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}

class MasterViewController: UIViewController
{
    @IBOutlet weak var calendarView: NWCalendarView!
    @IBOutlet weak var backgroundScroll: UIScrollView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var firstSeperationLabel: UILabel!
    @IBOutlet weak var classTypeSegmentControl: UISegmentedControl!
    @IBOutlet weak var secondSeperationLabel: UILabel!
    @IBOutlet weak var eventsTable: UITableView!
    @IBOutlet weak var noEventsLabel: UILabel!
    @IBOutlet weak var tableTopSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var autoCompleteResultsTable: UITableView!
    @IBOutlet weak var searchBarButton: UIBarButtonItem!
    
    var objects = NSMutableArray()
    var refreshControl: UIRefreshControl!
    var timer: NSTimer!
    var currentOrientation: String!
    var detailViewController: DetailViewController? = nil
    var currentShowingMonthNumberAsString: String?
    var currentMonthAsString: String?
    var autocompleteStrings = [String]()
    var isSearching: Bool!
    var currentShowingMonthInCalendar: String?
    var autoCompleteObjects = [String]()
    var currentVisibleMonthName: NSString!
    var eventExistsMonthName: NSString!
    var currentVisibleMonthComponents: NSDateComponents!
    var eventExistsMonthComponents: NSDateComponents!
    var monthsDifference: NSInteger!
    
    var allEventObjectsArray: NSMutableArray = []
    var allEventNames: NSMutableArray = []
    var currentMonthEventObjectsArray: NSMutableArray = []
    var allVisibleCalednarMonths: NSMutableArray = []
    var allEventLocations: NSMutableArray = []
    var allEventMonths: NSMutableArray = []
    var searchResults: NSMutableArray = []
    var autoCompleteSearchResults: NSMutableArray = []
    var isSearchByDay: Bool!
    var searchByDayResults: NSMutableArray = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var activityIndicator: UIActivityIndicatorView!;
    var trasnparentView: UIView!
    
    let kJSONUrl = "http://www.mocky.io/v2/56e00b8f100000490240f88f"
    
    //MARK: - View Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        showActivityIndicator()
        
        //Intital Setup
        initialSetup()
    }
    
    //MARK: - Helper Methods
    func initialSetup() //Allocations and Declarations
    {
        currentOrientation = "Portrait"
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate:NSDate()  as AnyObject as! NSDate)
        
        let month = components.month
        currentMonthAsString = String(month)
        
        for i in month..<13
        {
            let dateFormatter: NSDateFormatter = NSDateFormatter()
            let months = dateFormatter.monthSymbols
            let monthSymbol = months[i-1]
            
            //starts from current month
            allVisibleCalednarMonths .addObject(monthSymbol.uppercaseString)
        }
        
        //Set Segment Index to 0
        classTypeSegmentControl.selectedSegmentIndex = 0
        
        //Hide AutoComplete Table
        autoCompleteResultsTable.hidden = true
        
        //Set searchbar value to false
        isSearching = false
        
        //Set day search value to false
        isSearchByDay = false
        
        //Get Request
        makeGetRequest(kJSONUrl)
        
        //Set Background Color to Calendar
        calendarView.backgroundColor = UIColor.whiteColor()
        
        //For Bottom Border
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.blackColor().CGColor
        border.frame = CGRect(x: 0, y: calendarView.frame.size.height - width, width:  calendarView.frame.size.width, height: calendarView.frame.size.height)
        border.borderWidth = width
        calendarView.layer.addSublayer(border)
        calendarView.layer.masksToBounds = true
        
        //calendar functionality
        let date = NSDate()
        
        self.currentShowingMonthNumberAsString = String(month)
        
        calendarView.selectedDates = [date]
        calendarView.selectionRangeLength = 1 //Single Date selection
        calendarView.maxMonths = 12 //Number of months to show
        calendarView.delegate = self
        
        self.calendarView.createCalendar()
        
        calendarView.scrollToDate(date, animated: true)
                
        //segment tint color
        classTypeSegmentControl.tintColor = UIColor(colorLiteralRed: 155/255.0, green: 184.0/255.0, blue: 222.0/232.0, alpha: 1)
        
        //segment extension
        classTypeSegmentControl.setFontSize(13)
        
        //segment border color
        classTypeSegmentControl.layer.borderWidth = 1.0
        classTypeSegmentControl.layer.borderColor = UIColor.blackColor().CGColor
        
        //Search Controller
        searchController.searchBar.barTintColor = UIColor.lightGrayColor();
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        searchController.searchBar.backgroundColor = UIColor.clearColor()
        searchController.searchBar.placeholder = "Search by name"
        backgroundScroll .addSubview(searchController.searchBar)
        
        searchController.searchBar.hidden = true
        
        //Refresh Control
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: "startRefresh", forControlEvents: UIControlEvents.ValueChanged)
        eventsTable.addSubview(self.refreshControl)
    }
    
    // MARK: - Show Activity Indicator
    func showActivityIndicator()
    {
        trasnparentView = UIView(frame: CGRectMake(0, 0, view.frame.size.width, view.frame.size.height))
        trasnparentView.backgroundColor = UIColor.blackColor()
        trasnparentView.alpha = 0.5
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        activityIndicator.center = view.center
        trasnparentView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        self.navigationController?.view.addSubview(trasnparentView)
    }
    
    //MARK: - API Call
    func makeGetRequest(url: String)
    {
        //Remove Array objects before calling API
        allEventLocations.removeAllObjects()
        allEventMonths.removeAllObjects()
        allEventNames.removeAllObjects()
        searchResults.removeAllObjects()
        autoCompleteSearchResults.removeAllObjects()
        currentMonthEventObjectsArray.removeAllObjects()
        
        let urlPath = url
        guard let endpoint = NSURL(string: urlPath) else { print("Error creating endpoint");return }
        let request = NSMutableURLRequest(URL:endpoint)
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) -> Void in
            do {
                guard let dat = data else { throw JSONError.NoData }
                guard let jsonResult = try NSJSONSerialization.JSONObjectWithData(dat, options: []) as? NSDictionary else { throw JSONError.ConversionFailed }
                //print(jsonResult)
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.allEventObjectsArray = jsonResult.valueForKey("Events") as! NSMutableArray
                    self.getEventsForMonth(self.currentShowingMonthNumberAsString!)
                    
                    self.activityIndicator.stopAnimating()
                    self.trasnparentView.removeFromSuperview()
                }
                
            } catch let error as JSONError {
                print(error.rawValue)
            } catch {
                print(error)
            }
            }.resume()
    }
    
    //MARK: - Observe Device Orientation
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        if UIDevice.currentDevice().orientation.isLandscape.boolValue
        {
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone
            {
                searchBarButton.enabled = false
                searchController.searchBar.hidden = true
                searchController.searchBar.resignFirstResponder()
                self.backgroundScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width)
                self.backgroundScroll.contentOffset = CGPoint(x: 0, y: 260)
                self.eventsTable.contentOffset = CGPoint(x: 0, y: 0)
                currentOrientation = "Landscape"
                tableTopSpaceConstraint.constant = 40
            }
        }
        
        else
        {
            searchBarButton.enabled = true
            currentOrientation = "Portrait"
            self.backgroundScroll.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)
            tableTopSpaceConstraint.constant = 0
            self.backgroundScroll.contentOffset = CGPoint(x: 0, y: 0)
            self.eventsTable.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    
    //MARK: - Pull Refresh Begins
    func startRefresh()
    {
        showActivityIndicator()
        
        isSearching = false
        isSearchByDay = false
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate:NSDate()  as AnyObject as! NSDate)
        
        let month = components.month
        
        self.currentShowingMonthNumberAsString = String(month)
        
        makeGetRequest("http://www.mocky.io/v2/56dd49f0110000af00d085a0")
        refreshControl.attributedTitle = NSAttributedString(string: "Release to refresh")
        timer = NSTimer.scheduledTimerWithTimeInterval(2.5, target: self, selector: Selector("endRefresh"), userInfo: nil, repeats: false)
    }
    
    //MARK: - Pull Refresh Ends
    func endRefresh()
    {
        self.refreshControl.endRefreshing()
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate:NSDate()  as AnyObject as! NSDate)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let onlyMonthDateFormatter: NSDateFormatter = NSDateFormatter()
        onlyMonthDateFormatter.dateFormat = "MMMM"
        
        let tmpLabel = appDelegate.window!.viewWithTag(1000) as? UILabel
        
        var toMonth: NSDate!
        let unitFlags: NSCalendarUnit = [.Year, .Month, .Day, .Weekday, .Calendar]
        
        let eventExistsMonthName: NSString = tmpLabel!.text!
        toMonth = onlyMonthDateFormatter.dateFromString(eventExistsMonthName as String)
        
        let currentShowingMonthComponents: NSDateComponents = NSCalendar.usLocaleCurrentCalendar().components(unitFlags, fromDate:toMonth as NSDate)
        
        let difference: NSInteger = currentShowingMonthComponents.month - components.month
        
        if difference > 0
        {
            for _ in 0..<difference
            {
                calendarView.prevMonthPressed()
            }
        }
        
        self.performSelector("chagneRefreshTitle", withObject: self, afterDelay: 1.0)
        
        if currentOrientation == "Landscape"
        {
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone
            {
                backgroundScroll.contentOffset = CGPoint(x: 0, y: 260)
            }
        }
    }
    
    //MARK: - Change Pull Refresh Title
    func chagneRefreshTitle()
    {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
    }
    
    //MARK: - For Dismissing Searchbar
    override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?)
    {
        searchController.dismissViewControllerAnimated(true, completion: nil)
        searchController.searchBar.hidden = true
        autoCompleteResultsTable.hidden = true
        searchController.searchBar.resignFirstResponder()
    }
    
    //MARK: - Segment IBAction
    @IBAction func segmentValueChanged(sender: AnyObject)
    {
        let sortedViews = sender.subviews.sort({$0.frame.origin.x < $1.frame.origin.x})
        
        for (index, view) in sortedViews.enumerate()
        {
            if index == sender.selectedSegmentIndex
            {
                view.tintColor = UIColor(colorLiteralRed: 155/255.0, green: 184.0/255.0, blue: 222.0/232.0, alpha: 1)
            }
                
            else
            {
                view.tintColor = UIColor(colorLiteralRed: 229.0/255.0, green: 225.0/255.0, blue: 220.0/255.0, alpha: 1)
            }
        }
        
        if isSearching == false
        {
            let tempArray: NSMutableArray = []
            
            for i in 0..<currentMonthEventObjectsArray.count
            {
                if classTypeSegmentControl.selectedSegmentIndex == 0
                {
                    if currentMonthEventObjectsArray.objectAtIndex(i).objectForKey("isMyClasses")!.isEqualToString("true")
                    {
                        tempArray .addObject(currentMonthEventObjectsArray.objectAtIndex(i))
                    }
                }
                    
                else if classTypeSegmentControl.selectedSegmentIndex == 1
                {
                    tempArray .addObject(currentMonthEventObjectsArray.objectAtIndex(i))
                }
            }
            
            finalEventsArray(tempArray)
        }
            
        else if isSearching == true
        {
            let tempArray: NSMutableArray = []
            
            for i in 0..<searchResults.count
            {
                if classTypeSegmentControl.selectedSegmentIndex == 0
                {
                    if searchResults.objectAtIndex(i).objectForKey("isMyClasses")!.isEqualToString("true")
                    {
                        tempArray .addObject(searchResults.objectAtIndex(i))
                    }
                }
                    
                else if classTypeSegmentControl.selectedSegmentIndex == 1
                {
                    tempArray .addObject(searchResults.objectAtIndex(i))
                }
            }
            
            finalEventsArray(tempArray)
        }
    }
    
    //MARK: - Search Bar dismissed
    func showSearchearchBar()
    {
        searchController.searchBar.selectedScopeButtonIndex = 0
        searchController.searchBar.text = ""
        searchController.searchBar.hidden = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.scopeButtonTitles = ["Name", "Month", "Location"]
        searchController.searchBar.becomeFirstResponder()
    }
    
    //MARK: - Search IBAction
    @IBAction func searchAction(sender: UIBarButtonItem)
    {
        showSearchearchBar()
    }
    
    //MARK: - Events for current/selected month
    func getEventsForMonth(monthNumber: String)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let responseDataDateFormatter: NSDateFormatter = NSDateFormatter()
        responseDataDateFormatter.dateFormat = "d/MM/yyyy hh:mm a"
        
        appDelegate.eventDatesArray.removeAllObjects()
        currentMonthEventObjectsArray .removeAllObjects()
        //print(currentShowingMonthNumberAsString)
        
        let eventsMonthsArray: NSMutableArray = []
        
        let eventMonthsArrayAsNumber: NSMutableArray = []
        
        for i in 0..<allEventObjectsArray.count
        {
            allEventNames.addObject(((self.allEventObjectsArray.valueForKey("event_name")).objectAtIndex(i)).uppercaseString)
            allEventLocations.addObject(((self.allEventObjectsArray.valueForKey("location")).objectAtIndex(i)).uppercaseString)
            
            let calendar = NSCalendar.currentCalendar()
            let splitDateComponents = calendar.components([.Day , .Month , .Year], fromDate:responseDataDateFormatter.dateFromString(((self.allEventObjectsArray.valueForKey("event_dateTime")).objectAtIndex(i)) as! String) as! AnyObject as! NSDate)
            
            let eventMonth = splitDateComponents.month
            
            if !eventMonthsArrayAsNumber .containsObject(eventMonth)
            {
                eventMonthsArrayAsNumber .addObject(eventMonth)
            }
            
            if eventMonthsArrayAsNumber.count > 0
            {
                for i in 0..<eventMonthsArrayAsNumber.count
                {
                    let dateFormatter: NSDateFormatter = NSDateFormatter()
                    let months = dateFormatter.monthSymbols
                    let monthSymbol = months[eventMonthsArrayAsNumber.objectAtIndex(i) as! NSInteger]
                    
                    if !allEventMonths .containsObject(monthSymbol.uppercaseString)
                    {
                        allEventMonths .addObject(monthSymbol.uppercaseString)
                    }
                }
            }
            
            let unitFlags: NSCalendarUnit = [.Year, .Month, .Day, .Weekday, .Calendar]
            let components = NSCalendar.usLocaleCurrentCalendar().components(unitFlags, fromDate: responseDataDateFormatter.dateFromString(((self.allEventObjectsArray.valueForKey("event_dateTime")).objectAtIndex(i)) as! String) as! AnyObject as! NSDate)
            
            let month = components.month
            currentShowingMonthNumberAsString = String(month)
            eventsMonthsArray .addObject(currentShowingMonthNumberAsString!)
            
            if eventsMonthsArray.objectAtIndex(i) .isEqualToString(monthNumber)
            {
                let day = components.day
                let dayAsString = String(day)
                
                if !appDelegate.eventDatesArray .containsObject(dayAsString)
                {
                    appDelegate.eventDatesArray .addObject(dayAsString)
                }
                
                let currentMonthEventObjectsDict: NSDictionary = allEventObjectsArray.objectAtIndex(i) as! NSDictionary
                
                if isSearching == false
                {
                    currentMonthEventObjectsArray.insertObject(currentMonthEventObjectsDict, atIndex: 0);
                }
            }
        }
        
        if isSearching == false
        {
            let tempArray: NSMutableArray = []
            
            for i in 0..<currentMonthEventObjectsArray.count
            {
                if classTypeSegmentControl.selectedSegmentIndex == 0
                {
                    if currentMonthEventObjectsArray.objectAtIndex(i).objectForKey("isMyClasses")!.isEqualToString("true")
                    {
                        tempArray .addObject(currentMonthEventObjectsArray.objectAtIndex(i))
                    }
                }
                    
                else if classTypeSegmentControl.selectedSegmentIndex == 1
                {
                    tempArray .addObject(currentMonthEventObjectsArray.objectAtIndex(i))
                }
            }
            
            finalEventsArray(tempArray)
        }
    }
    
    //MARK: - Reload Table Data
    func finalEventsArray(eventsArray: NSMutableArray)
    {
        if eventsArray.count == 0
        {
            noEventsLabel.hidden = false
            eventsTable.hidden = true
        }
            
        else
        {
            noEventsLabel.hidden = true
            eventsTable.hidden = false
            self.eventsTable .reloadData()
        }
    }
    
    //MARK: - Search Functionality
    func searchEvents(substring: String)
    {
        if searchController.searchBar.selectedScopeButtonIndex == 0
        {
            autoCompleteSearchResults.removeAllObjects()
            
            for i in 0..<allEventObjectsArray.count
            {
                let allEventObjectsDict: NSDictionary = allEventObjectsArray.objectAtIndex(i) as! NSDictionary
                
                if ((allEventObjectsDict .objectForKey("event_name")!.uppercaseString).hasPrefix((searchController.searchBar.text?.uppercaseString)!))
                {
                    if !autoCompleteSearchResults .containsObject(allEventObjectsArray.objectAtIndex(i))
                    {
                        autoCompleteSearchResults.addObject(allEventObjectsArray.objectAtIndex(i))
                    }
                }
            }
            
            searchByArray(autoCompleteSearchResults)
        }
            
        else if searchController.searchBar.selectedScopeButtonIndex == 1
        {
            if allVisibleCalednarMonths .containsObject(searchController.searchBar.text!.uppercaseString)
            {
                let dateFormatter1: NSDateFormatter = NSDateFormatter()
                dateFormatter1.dateFormat = "MMMM"
                
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components([.Day , .Month , .Year], fromDate:dateFormatter1.dateFromString(searchController.searchBar.text!.uppercaseString)  as! AnyObject as! NSDate)
                
                let month = components.month
                let searchMonthNumberAsString: String = String(month)
                
                let myString: NSString = NSString(format: "%@/2016", searchMonthNumberAsString)
                
                print(myString)
                
                var seperationArray: NSArray!
                
                autoCompleteSearchResults.removeAllObjects()
                
                for i in 0..<allEventObjectsArray.count
                {
                    seperationArray = ((self.allEventObjectsArray.valueForKey("event_dateTime")).objectAtIndex(i)).componentsSeparatedByString(" ")
                    
                    if (seperationArray.objectAtIndex(0) .hasSuffix(myString as String))
                    {
                        if !autoCompleteSearchResults .containsObject(allEventObjectsArray.objectAtIndex(i))
                        {
                            autoCompleteSearchResults.addObject(allEventObjectsArray.objectAtIndex(i))
                        }
                    }
                }
                
                searchByArray(autoCompleteSearchResults)
            }
        }
            
        else if searchController.searchBar.selectedScopeButtonIndex == 2
        {
            autoCompleteSearchResults.removeAllObjects()
            
            for i in 0..<allEventObjectsArray.count
            {
                let allEventObjectsDict: NSDictionary = allEventObjectsArray.objectAtIndex(i) as! NSDictionary
                
                if ((allEventObjectsDict .objectForKey("location")!.uppercaseString).hasPrefix((searchController.searchBar.text?.uppercaseString)!))
                {
                    if !autoCompleteSearchResults .containsObject(allEventObjectsArray.objectAtIndex(i))
                    {
                        autoCompleteSearchResults.addObject(allEventObjectsArray.objectAtIndex(i))
                    }
                }
            }
            
            searchByArray(autoCompleteSearchResults)
        }
    }
    
    //MARK: - Search Results
    func searchByArray(arrayToPass: NSMutableArray)
    {
        if arrayToPass.count > 0
        {
            autoCompleteResultsTable.hidden = false
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.addSubview(autoCompleteResultsTable)
            autoCompleteResultsTable.reloadData()
        }
            
        else
        {
            autoCompleteResultsTable.hidden = true
        }
    }
}

//MARK: - Segment Control Extension
extension UISegmentedControl
{
    func setFontSize(fontSize: CGFloat)
    {
        if #available(iOS 8.2, *) {
            let normalTextAttributes: [NSObject : AnyObject] = [
                NSForegroundColorAttributeName: UIColor.blackColor(),
                NSFontAttributeName: UIFont.systemFontOfSize(fontSize, weight: UIFontWeightBold)
            ]
            
            self.setTitleTextAttributes(normalTextAttributes, forState: .Normal)
            self.setTitleTextAttributes(normalTextAttributes, forState: .Highlighted)
            self.setTitleTextAttributes(normalTextAttributes, forState: .Selected)
        } else {
            // Fallback on earlier versions
            self.setTitleTextAttributes([NSFontAttributeName: fontSize], forState: UIControlState.Normal)
            self.setTitleTextAttributes([NSFontAttributeName: fontSize], forState: UIControlState.Highlighted)
            self.setTitleTextAttributes([NSFontAttributeName: fontSize], forState: UIControlState.Selected)
        }
    }
}

//MARK: - TableView Delegates and Datasources Extension
extension MasterViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfSections section: Int) -> Int
    {
        
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var rowsCount: NSInteger!
        
        if tableView == eventsTable
        {
            if isSearching == false
            {
                rowsCount = currentMonthEventObjectsArray.count
            }
            
            if isSearching == true
            {
                rowsCount = searchResults.count
            }
            
            if isSearchByDay == true
            {
                rowsCount = searchByDayResults.count
            }
            
            return rowsCount
        }
            
        else
        {
            if autoCompleteSearchResults.count > 0
            {
                rowsCount = autoCompleteSearchResults.count
                return rowsCount
            }
                
            else
            {
                rowsCount = 0
            }
            
            return rowsCount
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        var rowHeight: CGFloat!
        
        rowHeight = 110
        
        return rowHeight
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        cell.contentView.backgroundColor = UIColor.clearColor()
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(0, 10, self.view.frame.size.width, 100))
        
        whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 2.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if tableView == eventsTable
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("Event_Cell", forIndexPath: indexPath)
            
            let eventDateLabel = cell.viewWithTag(100) as! UILabel
            let eventNameLabel = cell.viewWithTag(200) as! UILabel
            let userNameLabel = cell.viewWithTag(300) as! UILabel
            let locationLabel = cell.viewWithTag(400) as! UILabel
            
            if isSearching == false
            {
                let dateFormatter: NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "d/M/yyyy"
                
                let seperationArray: NSArray!
                seperationArray = currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("event_dateTime")?.componentsSeparatedByString(" ")
                
                let date: NSDate = dateFormatter.dateFromString(seperationArray.objectAtIndex(0) as! String)!
                
                let dateFormatter1: NSDateFormatter = NSDateFormatter()
                dateFormatter1.dateFormat = "EEEE"
                let dayOfWeekString = dateFormatter1.stringFromDate(date)
                
                let dayAsString = String(dayOfWeekString)
                let timeAsString = String(seperationArray.objectAtIndex(1))
                let timeFormatAsString = String(seperationArray.objectAtIndex(2))
                
                let eventDateTime = NSString(format:"%@ - %@ %@", dayAsString, timeAsString, timeFormatAsString)
                
                eventDateLabel.text = eventDateTime as String
                eventNameLabel.text = currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("event_name") as? String
                userNameLabel.text = currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("username") as? String
                locationLabel.text = currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("location") as? String
                
                if classTypeSegmentControl.selectedSegmentIndex == 0
                {
                    if currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("isMyClasses")!.isEqualToString("true")
                    {
                        cell.hidden = false
                    }
                    
                    if currentMonthEventObjectsArray.objectAtIndex(indexPath.row).objectForKey("isMyClasses")!.isEqualToString("false")
                    {
                        cell.hidden = true
                    }
                }
                    
                else if classTypeSegmentControl.selectedSegmentIndex == 1
                {
                    cell.hidden = false
                }
            }
                
            if isSearching == true
            {
                let dateFormatter: NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "d/M/yyyy"
                
                let seperationArray: NSArray!
                seperationArray = searchResults.objectAtIndex(indexPath.row).objectForKey("event_dateTime")?.componentsSeparatedByString(" ")
                
                let date: NSDate = dateFormatter.dateFromString(seperationArray.objectAtIndex(0) as! String)!
                
                let monthDateFormatter: NSDateFormatter = NSDateFormatter()
                monthDateFormatter.dateFormat = "EEEE"
                let dayOfWeekString = monthDateFormatter.stringFromDate(date)
                
                let dayAsString = String(dayOfWeekString)
                let timeAsString = String(seperationArray.objectAtIndex(1))
                let timeFormatAsString = String(seperationArray.objectAtIndex(2))
                
                let eventDateTime = NSString(format:"%@ - %@ %@", dayAsString, timeAsString, timeFormatAsString)
                
                eventDateLabel.text = eventDateTime as String
                
                eventNameLabel.text = searchResults.objectAtIndex(indexPath.row).objectForKey("event_name") as? String
                userNameLabel.text = searchResults.objectAtIndex(indexPath.row).objectForKey("username") as? String
                locationLabel.text = searchResults.objectAtIndex(indexPath.row).objectForKey("location") as? String
                
                if classTypeSegmentControl.selectedSegmentIndex == 0
                {
                    if searchResults.objectAtIndex(indexPath.row).objectForKey("isMyClasses")!.isEqualToString("true")
                    {
                        cell.hidden = false
                    }
                    
                    if searchResults.objectAtIndex(indexPath.row).objectForKey("isMyClasses")!.isEqualToString("false")
                    {
                        cell.hidden = true
                    }
                }
                    
                else if classTypeSegmentControl.selectedSegmentIndex == 1
                {
                    cell.hidden = false
                }
            }
            
            if isSearchByDay == true
            {
                let dateFormatter: NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "d/M/yyyy"
                
                let seperationArray: NSArray!
                seperationArray = searchByDayResults.objectAtIndex(indexPath.row).objectForKey("event_dateTime")?.componentsSeparatedByString(" ")
                
                let date: NSDate = dateFormatter.dateFromString(seperationArray.objectAtIndex(0) as! String)!
                
                let dateFormatter1: NSDateFormatter = NSDateFormatter()
                dateFormatter1.dateFormat = "EEEE"
                let dayOfWeekString = dateFormatter1.stringFromDate(date)
                
                let dayAsString = String(dayOfWeekString)
                let timeAsString = String(seperationArray.objectAtIndex(1))
                let timeFormatAsString = String(seperationArray.objectAtIndex(2))
                
                let eventDateTime = NSString(format:"%@ - %@ %@", dayAsString, timeAsString, timeFormatAsString)
                
                eventDateLabel.text = eventDateTime as String
                
                eventNameLabel.text = searchByDayResults.objectAtIndex(indexPath.row).objectForKey("event_name") as? String
                userNameLabel.text = searchByDayResults.objectAtIndex(indexPath.row).objectForKey("username") as? String
                locationLabel.text = searchByDayResults.objectAtIndex(indexPath.row).objectForKey("location") as? String
            }
            
            return cell
        }
            
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("AutoComplete_Cell", forIndexPath: indexPath)
            
            let eventDateLabel = cell.viewWithTag(100) as! UILabel
            let eventNameLabel = cell.viewWithTag(200) as! UILabel
            let userNameLabel = cell.viewWithTag(300) as! UILabel
            let locationLabel = cell.viewWithTag(400) as! UILabel
            
            if autoCompleteSearchResults.count > 0
            {
                let dateFormatter: NSDateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "d/M/yyyy"
                
                let seperationArray: NSArray!
                seperationArray = autoCompleteSearchResults.valueForKey("event_dateTime").objectAtIndex(indexPath.row).componentsSeparatedByString(" ")
                
                let date: NSDate = dateFormatter.dateFromString(seperationArray.objectAtIndex(0) as! String)!
                
                let dateFormatter1: NSDateFormatter = NSDateFormatter()
                dateFormatter1.dateFormat = "EEEE"
                let dayOfWeekString = dateFormatter1.stringFromDate(date)
                
                let dayAsString = String(dayOfWeekString)
                let timeAsString = String(seperationArray.objectAtIndex(1))
                let timeFormatAsString = String(seperationArray.objectAtIndex(2))
                
                let eventDateTime = NSString(format:"%@ - %@ %@", dayAsString, timeAsString, timeFormatAsString)
                
                eventDateLabel.text = eventDateTime as String
                eventNameLabel.text = autoCompleteSearchResults.valueForKey("event_name").objectAtIndex(indexPath.row) as? String
                userNameLabel.text = autoCompleteSearchResults.valueForKey("username").objectAtIndex(indexPath.row) as? String
                locationLabel.text = autoCompleteSearchResults.valueForKey("location").objectAtIndex(indexPath.row) as? String
            }
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
    {
        let cannotAttend = UITableViewRowAction(style: .Normal, title: "Cannot Attend") { action, index in
            print("Cannot Attend event")
        }
        
        cannotAttend.backgroundColor = UIColor.lightGrayColor()
        
        return [cannotAttend]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if tableView == eventsTable
        {
            if UIDevice.currentDevice().userInterfaceIdiom == .Phone
            {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.navigationController!.pushViewController(mainStoryboard.instantiateViewControllerWithIdentifier("DetailViewControllerStoryboard"), animated: true)
            }
        }
    }
}

//MARK: - Search Bar Delegates Extension
extension MasterViewController: UISearchResultsUpdating, UISearchBarDelegate
{
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        searchEvents(searchController.searchBar.text!)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.characters.count == 0
        {
            autoCompleteSearchResults.removeAllObjects()
            autoCompleteResultsTable.hidden = true
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
        autoCompleteResultsTable.hidden = true
        searchController.searchBar.hidden = true
        searchController.searchBar.resignFirstResponder()
        
        isSearching = false
        isSearchByDay = false
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
    {
        searchController.searchBar.text = ""
        autoCompleteResultsTable.hidden = true
        autoCompleteSearchResults.removeAllObjects()
        
        if searchBar.selectedScopeButtonIndex == 0
        {
            searchController.searchBar.selectedScopeButtonIndex = 0
            searchController.searchBar.placeholder = "Search by name"
        }
            
        else if searchBar.selectedScopeButtonIndex == 1
        {
            searchController.searchBar.selectedScopeButtonIndex = 1
            searchController.searchBar.placeholder = "Search by month"
        }
            
        else if searchBar.selectedScopeButtonIndex == 2
        {
            searchController.searchBar.selectedScopeButtonIndex = 2
            searchController.searchBar.placeholder = "Search by location"
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        searchController.searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar)
    {
        searchController.searchBar.resignFirstResponder()
    }
}

//MARK: - Canlendar Delegates
extension MasterViewController: NWCalendarViewDelegate
{
    func didChangeFromMonthToMonth(fromMonth: NSDateComponents, toMonth: NSDateComponents)
    {
        let date = NSDate()
        calendarView.selectedDates = [date]
        
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        
        let months = dateFormatter.standaloneMonthSymbols
        let fromMonthName = months[fromMonth.month-1] as String
        let toMonthName = months[toMonth.month-1] as String
        print("Change From '\(fromMonthName)' to '\(toMonthName)'")
        
        let currentShowingMonthNumber = toMonth.month
        currentShowingMonthNumberAsString = String(currentShowingMonthNumber)
        
        isSearching = false
        isSearchByDay = false
        
        getEventsForMonth(currentShowingMonthNumberAsString!)
    }
    
    func didSelectedDate(toDate: NSDateComponents)
    {
        let dayAsString = String(toDate.day)
        let monthAsString = String(toDate.month)
        let yearAsString = String(toDate.year)
        
        let selectedDate = NSString(format:"%@/%@/%@", dayAsString, monthAsString, yearAsString)
        
        print(selectedDate)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if !appDelegate.eventDatesArray .containsObject(dayAsString)
        {
            noEventsLabel.hidden = false
            eventsTable.hidden = true
        }
            
        else
        {
            isSearchByDay = true
            
            searchByDayResults.removeAllObjects()
            
            for i in 0..<currentMonthEventObjectsArray.count
            {
                let mystring:NSString = (currentMonthEventObjectsArray.objectAtIndex(i).objectForKey("event_dateTime") as? String)!
                
                
                if (mystring.containsString(selectedDate as String))
                {
                    searchByDayResults.addObject(currentMonthEventObjectsArray.objectAtIndex(i))
                }
            }
                        
            if searchByDayResults.count > 0
            {
                noEventsLabel.hidden = true
                eventsTable.hidden = false
                self.eventsTable .reloadData()
            }
        }
    }
}
